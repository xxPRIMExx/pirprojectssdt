﻿USE BookStore
Declare @004_InsertDataIntoTempBookAuthor varchar(50) ='004_InsertDataIntoTempBookAuthor';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @004_InsertDataIntoTempBookAuthor) = 0)
BEGIN
	Select ba.Id, ba.Book_Id, ba.Author_Id
	INTO #TempBookAuthor
	From BookAuthor ba

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@004_InsertDataIntoTempBookAuthor)
END