﻿USE BookStore

IF not exists (select * from sysobjects where name='ScriptsLog' and xtype='U')
	CREATE TABLE ScriptsLog
	(
		Id INT IDENTITY,
		ScriptName VARCHAR(50),
		CONSTRAINT PK_ScriptsLog_Id PRIMARY KEY CLUSTERED (Id)
	)
GO