﻿USE BookStore
Declare @002_DeleteFromBookAuthor varchar(50) ='002_DeleteFromBookAuthor';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @002_DeleteFromBookAuthor) = 0)
BEGIN
	
	DELETE from BookAuthor

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@002_DeleteFromBookAuthor)

END
