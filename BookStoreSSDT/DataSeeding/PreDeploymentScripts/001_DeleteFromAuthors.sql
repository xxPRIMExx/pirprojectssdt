﻿USE BookStore
Declare @001_DeleteFromAuthors varchar(50) ='001_DeleteFromAuthors';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @001_DeleteFromAuthors) = 0)
BEGIN

	DELETE from Authors;

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@001_DeleteFromAuthors);
	
END
