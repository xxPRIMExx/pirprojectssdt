﻿Use BookStore
Declare @003_InsertDataIIntoTempAuthors varchar(50) ='003_InsertDataIIntoTempAuthors';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @003_InsertDataIIntoTempAuthors) = 0)
BEGIN

	Select a.Id, a.FirstName, a.LastName
	INTO #TempAuthors
	From Authors a

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@003_InsertDataIIntoTempAuthors)

END
