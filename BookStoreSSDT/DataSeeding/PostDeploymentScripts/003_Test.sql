﻿USE BookStore
Declare @003_Test varchar(50) ='003_Test';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @003_Test) = 0)
BEGIN

	Insert INTO Authors(FirstName, LastName)
	VALUES ('SSDT-1', 'SSDT-1')

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@003_Test);
	
END
