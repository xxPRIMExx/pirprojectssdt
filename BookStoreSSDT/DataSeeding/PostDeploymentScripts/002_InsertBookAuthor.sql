﻿USE BookStore

Declare @002_InsertBookAuthor varchar(50) ='002_InsertBookAuthor';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @002_InsertBookAuthor) = 0)
BEGIN

	SET IDENTITY_INSERT BookAuthor ON

	Insert into BookAuthor(Id, Book_Id, Author_Id)
	SELECT Id, Book_Id, Author_Id
	From #TempBookAuthor

	SET IDENTITY_INSERT BookAuthor OFF

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@002_InsertBookAuthor)

END

