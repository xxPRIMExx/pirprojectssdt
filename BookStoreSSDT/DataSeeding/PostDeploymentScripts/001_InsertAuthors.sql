﻿USE BookStore

Declare @001_InsertAuthors varchar(50) ='001_InsertAuthors';

if((Select Count(*) From ScriptsLog sl Where sl.ScriptName = @001_InsertAuthors) = 0)
BEGIN
	
	SET IDENTITY_INSERT Authors ON

	INSERT INTO Authors(Id, FirstName, LastName)
	Select Id, FirstName, LastName
	FROM #TempAuthors

	SET IDENTITY_INSERT Authors OFF

	INSERT INTO ScriptsLog(ScriptName)
	VALUES (@001_InsertAuthors)

END
