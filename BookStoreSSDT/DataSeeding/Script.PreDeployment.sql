﻿/*
 Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут исполнены перед скриптом построения.	
 Используйте синтаксис SQLCMD для включения файла в скрипт, выполняемый перед развертыванием.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте перед развертыванием.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

:r .\PreDeploymentScripts\000_ScriptsLog.sql
:r .\PreDeploymentScripts\003_InsertDataIIntoTempAuthors.sql
:r .\PreDeploymentScripts\004_InsertDataIntoTempBookAuthor.sql
:r .\PreDeploymentScripts\002_DeleteFromBookAuthor.sql
:r .\PreDeploymentScripts\001_DeleteFromAuthors.sql

--Select *
--INTO #TempBookAuthor
--From BookAuthor

--DELETE from BookAuthor

--Select * 
--INTO #TempAuthors
--From Authors

--DELETE from Authors 