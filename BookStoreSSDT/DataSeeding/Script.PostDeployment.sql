﻿/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


:r .\PostDeploymentScripts\001_InsertAuthors.sql
:r .\PostDeploymentScripts\002_InsertBookAuthor.sql
:r .\PostDeploymentScripts\003_Test.sql

--USE BookStore
--SET IDENTITY_INSERT Authors ON

--INSERT INTO Authors(Id, FirstName, LastName)
--Select Id, FirstName, LastName
--FROM #TempAuthors

--SET IDENTITY_INSERT Authors OFF

--SET IDENTITY_INSERT BookAuthor ON

--Insert into BookAuthor(Id, Book_Id, Author_Id)
--SELECT Id, Book_Id, Author_Id
--From #TempBookAuthor

--SET IDENTITY_INSERT BookAuthor OFF