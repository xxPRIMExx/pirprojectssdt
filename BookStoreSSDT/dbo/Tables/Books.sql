﻿CREATE TABLE [dbo].[Books] (
    [Id]           INT          IDENTITY (1, 1) NOT NULL,
    [Name]         VARCHAR (50) NOT NULL,
    [CreationDate] DATETIME     NULL,
    [Rate]         INT          NULL,
    [PageCount]    INT          NULL,
    CONSTRAINT [PK_Books_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

