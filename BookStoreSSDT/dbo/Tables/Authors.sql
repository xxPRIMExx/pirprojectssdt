﻿CREATE TABLE [dbo].[Authors] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [FirstName] VARCHAR (50) NULL,
    [LastName]  VARCHAR (50) NULL,
    CONSTRAINT [PK_Authors_Id] PRIMARY KEY CLUSTERED ([Id] ASC)
);

