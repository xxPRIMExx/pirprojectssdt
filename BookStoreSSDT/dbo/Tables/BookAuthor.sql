﻿CREATE TABLE [dbo].[BookAuthor] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [Book_Id]   INT NOT NULL,
    [Author_Id] INT NOT NULL,
    CONSTRAINT [PK_BookAuthor_Id] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_BookAuthor_AuthorId] FOREIGN KEY ([Author_Id]) REFERENCES [dbo].[Authors] ([Id]),
    CONSTRAINT [FK_BookAuthor_BookId] FOREIGN KEY ([Book_Id]) REFERENCES [dbo].[Books] ([Id])
);

